'use strict';

module.exports = {
    parser: 'babel-eslint',
    'env': {
        'browser': true,
        'es6': true,
    },
    plugins: [
        'html',
        '@tivac/svelte'
    ],
    settings: {
        'html/html-extensions': ['.html', '.svelte'],
    },
    extends: [
        'eslint:recommended',
        'plugin:@tivac/svelte/svelte',
    ],

    rules: {
        'indent': ['error', 4],
        'linebreak-style': ['error', 'unix'],
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'],
        'comma-dangle': ['error', 'always-multiline'],
        'no-console': ['off'],
        'no-extra-semi': ['off'],

        '@tivac/svelte/property-ordering': [
            'error', {
                order: [
                    'namespace',
                    'tag',
                    'components',
                    'props',
                    'events',
                    'immutable',
                    'data',
                    'store',
                    'computed',
                    'actions',
                    'transitions',
                    'helpers',
                    'oncreate',
                    'onstate',
                    'onupdate',
                    'ondestroy',
                    'preload',
                    'methods',
                    'setup',
                ]
            }
        ]
    },
};