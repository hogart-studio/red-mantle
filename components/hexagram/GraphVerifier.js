export class GraphVerifier {
    /**
     *
     * @param {Array<[number, number]>} expectedLines
     * @param {Function} onSuccess
     * @param {Function} onFail
     */
    constructor(expectedLines, onSuccess, onFail) {
        this.expectedLines = expectedLines.sort(this.compareLines);
        this.onSuccess = onSuccess;
        this.onFail = onFail;
        this.lines = [];
    }

    compareLines(line1, line2) {
        if (line1[0] === line2[0]) {
            return line1[1] - line2[1];
        } else {
            return line1[0] - line2[0];
        }
    }

    checkLinesArray() {
        for (let i = 0, len = this.lines.length; i < len; i++) {
            if (this.lines[i][0] !== this.expectedLines[i][0] || this.lines[i][1] !== this.expectedLines[i][1]) {
                return false;
            }
        }

        return true;
    }

    addLine(index1, index2) {
        if (index1 > index2) {
            this.lines.push([index2, index1]);
        } else {
            this.lines.push([index1, index2]);
        }

        this.lines.sort(this.compareLines);

        if (this.lines.length === this.expectedLines.length) {
            if (this.checkLinesArray()) {
                this.onSuccess();
            } else {
                this.lines = [];
                this.onFail();
            }
        }
    }
}