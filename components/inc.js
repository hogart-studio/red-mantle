export function inc(fieldName, inc = 1) {
    this.set({
        [fieldName]: this.get()[fieldName] + inc,
    });
}