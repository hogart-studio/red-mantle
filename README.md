# «Красная мантия»

Паззло-ориентированная текстовая игра.

## Лицензия

Кодовая часть лицензирована под [MIT](https://spdx.org/licenses/MIT.html). Текстовая часть лицензирована под [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
Файлы src/NotoColorEmoji.ttf и src/NotoSansRunic-Regular.ttf являются частью проекта [Noto](https://www.google.com/get/noto/) и лицензируются соответственно.

## Запуск и разработка

В системе должны быть установлены [butler](https://itch.io/docs/butler/), [tweego](https://www.motoslave.net/tweego/) и [nodejs/npm](https://nodejs.org/download/).

Игра использует standalone-компоненты, написанные на [svelte](https://svelte.technology/). Если планируется их изменение, после клонирования репозитория необходимо выполнить команду `npm install`. Команда `npm run svelte-dev` запускает сборщик в режиме компиляции на лету, а команда `npm run svelte-prod` совершает единоразовую сборку. Артефакты сборки находятся в `components/build` и копируются в код игры вручную.

Команда `npm run build` создает папку с собранной версией игры. `npm run deploy` выполняет сборку и деплой на itch.io.