/* eslint-env node */
import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import { eslint } from 'rollup-plugin-eslint';

const isProduction = process.env.BUILD === 'production';

const sveltePlugin = svelte({
    // By default, all .svelte and .html files are compiled
    extensions: ['.svelte'],
    legacy: false,
});

const resolvePlugin = resolve();

export default {
    input: ['./components/entry.js'],
    output: {
        file: './components/build/components.js',
        format: 'iife',
        name: 'redMantle',
    },
    plugins: [
        eslint({
            formatter: 'unix',
        }),
        sveltePlugin,
        resolvePlugin,
        isProduction && terser({
            sourcemap: false,
        }),
    ],
};